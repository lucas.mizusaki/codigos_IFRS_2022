#include <stdio.h>
#include <stdlib.h>

typedef struct nodo {
    int val;
    struct nodo *prox;    
    } nodo;


int main()
{ int valor;
  int i;
  struct nodo *indice;
  struct nodo *head;
  
  head = malloc (sizeof(nodo));
  indice = head;
  indice -> val = 0;
  indice -> prox = NULL;

  for (i = 1; i< 10 ; i++) {
    indice -> prox = malloc (sizeof(nodo));
    indice = indice -> prox;    
    indice -> val = i;
    indice -> prox = NULL;
  }
  indice = head;

  while (indice != NULL) {
    printf ("%d ", indice -> val);
    indice = indice -> prox;
  }

    return 0;
}
