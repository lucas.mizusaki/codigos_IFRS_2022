import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;

public class ProgramaGrafico {
    public static void main(String[] args) {
        // Primeiro, inicializamos o JFrame, a janela do programa
        JFrame janela = new JFrame("Meu Programa");
        // O Frame é configurado, recebendo tamanho e a configuracao de terminar o programa ao ser fechado.
        janela.setSize(200,300);
        janela.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );    

        //Aqui vamos colocar os elementos que vamos inserir no nosso programa
        JLabel label1 = new JLabel("Digite o numero para fatorial");
        JButton botao = new JButton();
        JTextField input = new JTextField(5);

        // Para posicionar os elementos (widgets) na tela, vamos criar um painel
        JPanel meuPainel = new JPanel();
        meuPainel.add(label1);
        meuPainel.add(input);
        meuPainel.add(botao);

        //Setar o painel como conteudo da janela e exibi-la
        janela.setContentPane(meuPainel);
        janela.setVisible(true);

    }
}

